package com.ty.domesticexecutivesuite.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ty.domesticexecutivesuite.dto.UserMaster;
import com.ty.domesticexecutivesuite.service.UserMasterService;
import com.ty.domesticexecutivesuite.util.ResponseStructure;

@RestController
public class UserMasterController {
	@Autowired
	UserMasterService masterService;

	@PostMapping("usermaster/{empid}")
	public ResponseEntity<ResponseStructure<UserMaster>> saveUserMaster(@PathVariable String empid,
			@RequestBody UserMaster usermaste) {
		return masterService.saveUserMaster(empid, usermaste);
	}

	@GetMapping("usermaster/{userMaster_id}")
	public ResponseEntity<ResponseStructure<UserMaster>> getUserMasterById(@PathVariable String userMaster_id) {
		return masterService.getUserMasterById(userMaster_id);
	}

	@GetMapping("usermaster")
	public ResponseEntity<ResponseStructure<List<UserMaster>>> getAllUserMaster() {
		return masterService.getAllUserMaster();
	}

	@PutMapping("usermaster/{userMaster_id}")
	public ResponseEntity<ResponseStructure<UserMaster>> updateUserMaster(@PathVariable String userMaster_id,
			UserMaster usermaster) {
		return masterService.updateUserMaster(userMaster_id, usermaster);
	}

	@GetMapping("userobject")
	public UserMaster iamUserMasterObject() {
		return new UserMaster();
	}
}
