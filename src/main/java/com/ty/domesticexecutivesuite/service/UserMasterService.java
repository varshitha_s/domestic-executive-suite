package com.ty.domesticexecutivesuite.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.domesticexecutivesuite.dto.UserMaster;
import com.ty.domesticexecutivesuite.util.ResponseStructure;

public interface UserMasterService {
	public ResponseEntity<ResponseStructure<UserMaster>> saveUserMaster(String empid, UserMaster usermaste);

	public ResponseEntity<ResponseStructure<UserMaster>> getUserMasterById(String userMaster_id);

	public ResponseEntity<ResponseStructure<List<UserMaster>>> getAllUserMaster();

	public ResponseEntity<ResponseStructure<UserMaster>> updateUserMaster(String userMaster_id, UserMaster usermaster);
}
