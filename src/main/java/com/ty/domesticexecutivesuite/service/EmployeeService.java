package com.ty.domesticexecutivesuite.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.domesticexecutivesuite.dto.Employee;
import com.ty.domesticexecutivesuite.util.ResponseStructure;

public interface EmployeeService {
	public ResponseEntity<ResponseStructure<Employee>> saveEmployee(Employee employee);
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeById(int empId);
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByFirstName(String firstName);
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByLastName(String lastName);
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByUnit(int unitId);
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByGrade(String gradeCode);
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByMaritailStatus(String maritialStatus);
	public ResponseEntity<ResponseStructure<List<Employee>>> getAllEmployees(); 
}
