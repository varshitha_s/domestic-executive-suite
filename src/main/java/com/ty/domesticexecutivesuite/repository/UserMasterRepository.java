package com.ty.domesticexecutivesuite.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.domesticexecutivesuite.dto.UserMaster;

public interface UserMasterRepository extends JpaRepository<UserMaster, String> {

}
