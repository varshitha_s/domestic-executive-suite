package com.ty.domesticexecutivesuite.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.domesticexecutivesuite.dto.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String>{

}
