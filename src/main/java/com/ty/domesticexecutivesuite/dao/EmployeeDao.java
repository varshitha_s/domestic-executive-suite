package com.ty.domesticexecutivesuite.dao;

import java.util.List;

import com.ty.domesticexecutivesuite.dto.Employee;

public interface EmployeeDao {
	public Employee saveEmployee(Employee employee);
	public Employee getEmployeeById(int id);
	public Employee getEmployeeByFirstName(String firstName);
	public Employee getEmployeeByLastName(String lastName);
	public Employee getEmployeeByUnit(int unitId);
	public Employee getEmployeeByGrade(String gradeCode);
	public Employee getEmployeeByMaritialStatus(String status);
	public List<Employee> getAllEmployee();
	public Employee updateEmployee(int id,Employee employee);
}
