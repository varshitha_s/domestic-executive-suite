package com.ty.domesticexecutivesuite.dao;

import java.util.List;

import com.ty.domesticexecutivesuite.dto.UserMaster;

public interface UserMasterDao {
	public UserMaster saveUserMaster(String empid, UserMaster usermaster);

	public UserMaster getUserMasterById(String userMaster_id);

	public List<UserMaster> getAllUserMaster();

	public UserMaster updateUserMaster(String userMaster_id,UserMaster usermaster );

}
