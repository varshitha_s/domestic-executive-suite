package com.ty.domesticexecutivesuite.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.domesticexecutivesuite.dao.UserMasterDao;
import com.ty.domesticexecutivesuite.dto.UserMaster;
import com.ty.domesticexecutivesuite.service.UserMasterService;
import com.ty.domesticexecutivesuite.util.ResponseStructure;

@Service
public class UserMasterServiceimpls implements UserMasterService {
	@Autowired
	UserMasterDao dao;

	@Override
	public ResponseEntity<ResponseStructure<UserMaster>> saveUserMaster(String empid, UserMaster usermaste) {
		ResponseStructure<UserMaster> structure = new ResponseStructure<>();
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("Sucess");
		structure.setData(dao.saveUserMaster(empid, usermaste));
		ResponseEntity<ResponseStructure<UserMaster>> responseEntity = new ResponseEntity<ResponseStructure<UserMaster>>(
				structure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<UserMaster>> getUserMasterById(String userMaster_id) {
		ResponseStructure<UserMaster> structure = new ResponseStructure<>();
		ResponseEntity<ResponseStructure<UserMaster>> responseEntity = null;
		UserMaster master = dao.getUserMasterById(userMaster_id);
		if (master != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Sucess");
			structure.setData(dao.getUserMasterById(userMaster_id));
			responseEntity = new ResponseEntity<ResponseStructure<UserMaster>>(structure, HttpStatus.OK);
		} else {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("ID not found");
			structure.setData(null);
			responseEntity = new ResponseEntity<ResponseStructure<UserMaster>>(structure, HttpStatus.NOT_FOUND);
		}
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<List<UserMaster>>> getAllUserMaster() {
		ResponseStructure<List<UserMaster>> structure = new ResponseStructure<>();
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("Sucess");
		structure.setData(dao.getAllUserMaster());
		return null;
	}

	@Override
	public ResponseEntity<ResponseStructure<UserMaster>> updateUserMaster(String userMaster_id, UserMaster usermaster) {
		ResponseStructure<UserMaster> structure = new ResponseStructure<>();
		ResponseEntity<ResponseStructure<UserMaster>> responseEntity = null;
		UserMaster userMaster2 = dao.getUserMasterById(userMaster_id);
		if (userMaster2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("Sucess");
			structure.setData(dao.updateUserMaster(userMaster_id, usermaster));
			responseEntity = new ResponseEntity<ResponseStructure<UserMaster>>(structure, HttpStatus.OK);
		} else {
			structure.setStatus(HttpStatus.NOT_FOUND.value());
			structure.setMessage("notSucess");
			structure.setData(null);
			responseEntity = new ResponseEntity<ResponseStructure<UserMaster>>(structure, HttpStatus.NOT_FOUND);
		}
		return responseEntity;
	}
}
