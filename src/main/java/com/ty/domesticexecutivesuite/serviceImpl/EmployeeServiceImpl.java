package com.ty.domesticexecutivesuite.serviceImpl;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.domesticexecutivesuite.dto.Employee;
import com.ty.domesticexecutivesuite.service.EmployeeService;
import com.ty.domesticexecutivesuite.util.ResponseStructure;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Override
	public ResponseEntity<ResponseStructure<Employee>> saveEmployee(Employee employee) {
		
		return null;
	}

	@Override
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeById(int empId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByFirstName(String firstName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByLastName(String lastName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByUnit(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByGrade(String gradeCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<ResponseStructure<Employee>> getEmployeeByMaritailStatus(String maritialStatus) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<ResponseStructure<List<Employee>>> getAllEmployees() {
		// TODO Auto-generated method stub
		return null;
	}

}
