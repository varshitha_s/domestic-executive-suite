package com.ty.domesticexecutivesuite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomesticExecutiveSuiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomesticExecutiveSuiteApplication.class, args);
	}

}
