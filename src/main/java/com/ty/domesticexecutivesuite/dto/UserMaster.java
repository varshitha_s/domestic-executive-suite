package com.ty.domesticexecutivesuite.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.ty.domesticexecutivesuite.util.UserMasterIdGenration;

@Entity
public class UserMaster {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emp_seq")
	@GenericGenerator(name = "emp_seq", strategy = "com.ty.domesticexecutivesuite.util", parameters = {
			@Parameter(name = UserMasterIdGenration.INCREMENT_PARAM, value = "50"),
			@Parameter(name = UserMasterIdGenration.VALUE_PREFIX_PARAMETER, value = "EMP"),
			@Parameter(name = UserMasterIdGenration.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	private String id;
	private String user_name;
	private String password;
	private String user_type;
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

}
