package com.ty.domesticexecutivesuite.daoImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.ty.domesticexecutivesuite.dao.UserMasterDao;
import com.ty.domesticexecutivesuite.dto.UserMaster;
import com.ty.domesticexecutivesuite.repository.UserMasterRepository;


@Repository
public class UserMasterDaoimpl implements UserMasterDao {
	@Autowired
	UserMasterRepository repository;

	@Override
	public UserMaster saveUserMaster(String empid, UserMaster usermaster) {
		return repository.save(usermaster);
	}

	@Override
	public UserMaster getUserMasterById(String userMaster_id) {
		Optional<UserMaster> user = repository.findById(userMaster_id);
		if (user != null) {
			return user.get();
		} else
			return null;
	}

	@Override
	public List<UserMaster> getAllUserMaster() {
		return repository.findAll();
	}

	@Override
	public UserMaster updateUserMaster(String userMaster_id, UserMaster usermaster) {
		UserMaster existing = getUserMasterById(userMaster_id);
		if(existing!=null) {
			existing.setUser_name(usermaster.getUser_name());
			existing.setPassword(usermaster.getPassword());
			existing.setUser_type(usermaster.getUser_type());
		return repository.save(existing);
		}
		return null;
	}

}
