package com.ty.domesticexecutivesuite.daoImpl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ty.domesticexecutivesuite.dao.EmployeeDao;
import com.ty.domesticexecutivesuite.dto.Employee;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public Employee saveEmployee(Employee employee) {
		return null;
	}

	@Override
	public Employee getEmployeeById(int id) {
		return null;
	}

	@Override
	public Employee getEmployeeByFirstName(String firstName) {
		return null;
	}

	@Override
	public Employee getEmployeeByLastName(String lastName) {
		return null;
	}

	@Override
	public Employee getEmployeeByUnit(int unitId) {
		return null;
	}

	@Override
	public Employee getEmployeeByGrade(String gradeCode) {
		return null;
	}

	@Override
	public Employee getEmployeeByMaritialStatus(String status) {
		return null;
	}

	@Override
	public List<Employee> getAllEmployee() {
		return null;
	}

	@Override
	public Employee updateEmployee(int id, Employee employee) {
		return null;
	}

}
